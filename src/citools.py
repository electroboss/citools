from copy import deepcopy as dc

alpha = "abcdefghijklmnopqrstuvwxyz"
MORSEDICT = {
  "-----": "0",
  ".----": "1",
  "..---": "2",
  "...--": "3",
  "....-": "4",
  ".....": "5",
  "-....": "6",
  "--...": "7",
  "---..": "8",
  "----.": "9",
  ".-": "a",
  "-...": "b",
  "-.-.": "c",
  "-..": "d",
  ".": "e",
  "..-.": "f",
  "--.": "g",
  "....": "h",
  "..": "i",
  ".---": "j",
  "-.-": "k",
  ".-..": "l",
  "--": "m",
  "-.": "n",
  "---": "o",
  ".--.": "p",
  "--.-": "q",
  ".-.": "r",
  "...": "s",
  "-": "t",
  "..-": "u",
  "...-": "v",
  ".--": "w",
  "-..-": "x",
  "-.--": "y",
  "--..": "z",
  ".-.-.-": ".",
  "--..--": ",",
  "..--..": "?",
  "-.-.--": "!",
  "-....-": "-",
  "-..-.": "/",
  ".--.-.": "@",
  "-.--.": "(",
  "-.--.-": ")",
  "/": " "
}


def monosub(code: str, cipher: str, reverse: bool=False) -> str:
  """
  Substitute the monoalphabetically ciphered "code" using the cipher "cipher". Set reverse to True for encode.
  """

  out = ""
  if reverse:
    for x in code:
      if x.lower() in alpha:
        out += alpha[cipher.index(x.lower())]
      else:
        out += x
  else:
    for x in code:
      if x.lower() in alpha:
        out += cipher[ord(x.lower())-ord("a")]
      else:
        out += x
  return out

def caesar(code: str, shift: int) -> str:
  """
  Caesar shift "code" by "shift".
  """
  out = ""
  for x in code:
    if x.lower() in alpha:
      out += chr((ord(x.lower())-ord("a")+shift)%26+ord("a"))
    else:
      out += x
  return out

def all_caesar(code: str) -> str:
  """
  Caesar shift file "code" by every value, starting from 0 through to 25.
  """
  out = []
  for i in range(26):
    out.append(caesar(code, i))
  return out

def freqanalyse(code: str, letters: int) -> str:
  """
  Frequency analyse file "code" with letter sequences of "letters" length.
  E.g.
    freqanalyse("aaaa",1) → {"a":4}
    freqanalyse("ababab",2) → {"ab":3, "ba":2}
  """
  combos = get_combos(letters)
  
  freqs = dict()
  for combo in combos:
    freqs[combo]=code.count(combo)
  return dict(sorted(freqs.items(), key=lambda x:x[1],reverse=True))

def get_combos(letters):
  combos = list(alpha)
  for _ in range(letters-1):
    combos2=dc(combos)
    combos=[]
    for i,x in enumerate(combos2):
      for y in alpha:
        combos.append(x+y)
  return combos

def generate_keyword_cipher(keyword: str, alphabet: str="abcdefghijklmnopqrstuvwxyz") -> str:
  """
  Generate a keyword cipher of keyword "keyword" (based of alphabet "alphabet").
  E.g.
    generate_keyword_cipher("nose","qwertyuiopasdfghjklzxcvbnm") → "noseqwrtyuipadfghjklzxcvbm"
  """
  # Check is a valid keyword
  for x in alpha:
    if keyword.count(x) > 1:
      raise Exception("Keyword must have at most 1 of each letter.")
  for x in keyword:
    if x not in alpha:
      raise Exception("Keyword must have at most 1 of each letter.")
  # Turn into cipher
  cipher = keyword
  for x in alpha:
    if x not in cipher:
      cipher+=x
      break
  return cipher

def keyword_substitute(code: str, keyword: str) -> str:
  """
  Keyword substitute file "code" with keyword "keyword".
  """
  return monosub(code, generate_keyword_cipher(keyword=keyword))

def vigenere(code: str, keyword: str, reverse: bool = False) -> str:
  klen = len(keyword)
  code = code.lower()
  out = ""
  i = 0
  if reverse:
    for x in code:
      if x.lower() in alpha:
        out += chr((ord(x)-ord("a")+ord(keyword[i%klen])-ord('a'))%26+ord("a"))
        i += 1
      else:
        out += x
  else:
    for x in code:
      if x.lower() in alpha:
        out += chr((26+ord(x)-ord(keyword[i%klen]))%26+ord("a"))
        i += 1
      else:
        out += x
  
  return out

def vigenere_key_elimination(code, knowntext, keywordlen):
  """
  Vigenere key elimination - https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher#Key_elimination
  Returns None if knowntext not in code
  Else returns keyword
  """
  knowntext_delta = shift_delta(knowntext, keywordlen)
  code_delta = shift_delta(code, keywordlen)
  if knowntext_delta not in code_delta:
    return
  knownindexes = all_substr_indexes(code_delta,knowntext_delta)
  outs = []
  for knownindex in knownindexes:
    knownsegment = code[knownindex:knownindex+len(knowntext)-keywordlen]
    outs.append("".join([chr((26+ord(knownsegment[i])-ord(knowntext[i]))%26+ord("a")) for i in range(len(knowntext)-keywordlen)]))
  return outs

def transposition(code, keyword, reverse: bool=False):
  keyword = list(enumerate(keyword))
  keyword.sort(key=lambda x:x[1])
  keyword = [x[0] for x in keyword]
  keylen = len(keyword)
  out = ""
  if reverse:
    for i in range(len(code)//keylen):
      chunk = code[i*keylen:(i+1)*keylen]
      outchunk = [0]*keylen
      for j in range(keylen):
        outchunk[keyword.index(j)] = chunk[j]
      out += "".join(outchunk)
  else:
    for i in range(len(code)//keylen):
      chunk = code[i*keylen:(i+1)*keylen]
      outchunk = [0]*keylen
      for j in range(keylen):
        outchunk[keyword[j]] = chunk[j]
      out += "".join(outchunk)
  return out

def shift_delta(inputstring, shift):
  """
  Not bothered to explain this, it's nearly useless.
  """
  return "".join([chr((26+ord(inputstring[i])-ord(inputstring[i+shift]))%26+ord("a")) for i in range(len(inputstring)-shift)])

def all_substr_indexes(string, substring):
  return [i for i in range(len(string)) if string.startswith(substring, i)]

def all_possible_arrangements(letters: int):
  if letters == 1:
    return [[0]]
  prevarrangements = all_possible_arrangements(letters-1)
  arrangements = []
  for i in range(letters):
    for y in prevarrangements:
      y = [k+1 if k>=i else k for k in y]
      arrangements.append(y+[i])
  return arrangements

def morse(code: str) -> str:
  code = code.split(" ")
  out = ""
  for letter in code:
    out += MORSEDICT[letter]
  return out

def vigenere_freqanalyse(code, kwlen):
  """
    Vigenere frequency analysis. Not refactored.
  """
  return {x:freqanalyse([code[y*kwlen+x] for y in range(len(code)//kwlen)],1) for x in range(kwlen)}

def vigenere_keyword_from_freqs(freqs):
  """
    Vigenere keyword from its frequencies. Not optimised.
  """
  return vigenere_key_elimination(freqs,"e"*len(freqs),0)

def index_of_coincidence(k,code):
  """
    Index of coincidence. Not refactored.
  """

  def spaghet(x,y,code):
    if y == -1:
      return [freqanalyse(a, 1) for a in gesammtkunswerk(x,code)]
    return [list(freqanalyse(a, 1).keys())[y] for a in gesammtkunswerk(x,code)]
  
  def gesammtkunswerk(x,code):
    codes=[""]*x
    for i in range(len(code)//x):
      for j in range(x):
        codes[j]+=code[i*x+j]
    return codes

  freqs = spaghet(k,-1,code)
  n = len(code)//k
  return [sum([(freqs[y][x]*(freqs[y][x]-1))/(n*(n-1)) for x in alpha]) for y in range(k)]


if __name__ == "__main__":
  import cli
  cli.main()
