import argparse, citools

def main():
  parser = argparse.ArgumentParser(
    prog="citools",
    description="Cipher tools - Tools for cracking ciphers."
  )
  parser.add_argument(
    "-o", "--out",
    help="Output file",
    type=argparse.FileType("w+")
  )
  parser.set_defaults(func=lambda x:parser.print_help())
  subparser = parser.add_subparsers(help="Description")

  # Monosub
  monosub = subparser.add_parser("monosub", help="Monoalphabetic substitution")
  monosub.set_defaults(func=monosub_wrapper)
  monosub.add_argument(
    "-i", "--input",
    default="input.txt",
    help="Input file.",
    type=argparse.FileType("r"),
  )
  monosub.add_argument(
    "cipher",
    help="Cipher",
    type=str,
  )
  monosub.add_argument(
    "-a", "--alphabet",
    help="Alphabet upon which the cipher is based. This one [0] → cipher [0].",
    type=str,
    default="abcdefghijklmnopqrstuvwxyz"
  )

  # Caesar
  caesar = subparser.add_parser("caesar", help="Caesar shift")
  caesar.set_defaults(func=caesar_wrapper)
  caesar.add_argument(
    "-i", "--input",
    default="input.txt",
    help="Input file.",
    type=argparse.FileType("r"),
  )
  caesar.add_argument(
    "shift",
    help="Number to shift by. a/all for every number."
  )

  # Frequency analysis
  freqanalyse = subparser.add_parser("freq", help="Frequency analysis")
  freqanalyse.set_defaults(func=freqanalyse_wrapper)
  freqanalyse.add_argument(
    "-i", "--input",
    default="input.txt",
    help="Input file",
    type=argparse.FileType("r"),
  )
  freqanalyse.add_argument(
    "-l", "--letters",
    default=1,
    help="number of letters to analyse. Number of aa,ab...zz vs number of a,b...z.",
    type=int
  )

  # Keyword ciphers
  keyword = subparser.add_parser("keyword", help="Keyword ciphers")
  keyword_subparser = keyword.add_subparsers()
  # gen cipher
  keyword_gencipher = keyword_subparser.add_parser("gencipher", help="Generate a keyword cipher")
  keyword_gencipher.set_defaults(func=keyword_gencipher_wrapper)
  keyword_gencipher.add_argument(
    "keyword",
    type=str,
    help="Keyword to generate the cipher of"
  )
  keyword_gencipher.add_argument(
    "-a", "--alphabet",
    help="Alphabet upon which the cipher is based.",
    type=str,
    default="abcdefghijklmnopqrstuvwxyz"
  )
  # Monosub with a keyword
  keyword_substitute = keyword_subparser.add_parser("monosub", help="Substitute with a monoalphabetic keyword cipher")
  keyword_substitute.set_defaults(func=keyword_substitute_wrapper)
  keyword_substitute.add_argument(
    "keyword",
    type=str,
    help="Keyword to generate the cipher of"
  )
  keyword_substitute.add_argument(
    "-i", "--input",
    default="input.txt",
    help="Input file",
    type=argparse.FileType("r")
  )

  # Vigenère
  vigenere = subparser.add_parser("vigenere", help="Vigenère ciphers")
  vigenere_subparser = vigenere.add_subparsers()
  # Encode/decode
  vigenere_substitute = vigenere_subparser.add_parser("substitute", help="Substitute with a vigenère cipher")
  vigenere_substitute.set_defaults(func=vigenere_substitute_wrapper)
  vigenere_substitute.add_argument(
    "keyword",
    type=str,
    help="Keyword to decipher with"
  )
  vigenere_substitute.add_argument(
    "-i", "--input",
    default="input.txt",
    help="Input file",
    type=argparse.FileType("r"),
  )
  vigenere_substitute.add_argument(
    "-r", "--reverse",
    action="store_true",
    default=False,
    help="Reverse - i.e. encode the cipher rather than breaking it.",
  )
  # Key elimination
  vigenere_key_elimination = vigenere_subparser.add_parser("keyelim", help="Run key elimination\nhttps://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher#Key_elimination")
  vigenere_key_elimination.set_defaults(func=vigenere_key_elimination_wrapper)
  vigenere_key_elimination.add_argument(
    "-i", "--input",
    default="input.txt",
    help="Input file",
    type=argparse.FileType("r"),
  )
  vigenere_key_elimination.add_argument(
    "knowntext",
    help="Text known to be in the cipher",
    type=str
  )
  vigenere_key_elimination.add_argument(
    "keywordlen",
    help="Length of keyword. Use a Kasiski examination to get it.\nhttps://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher#Kasiski_examination",
    type=int
  )


  args = parser.parse_args()
  args.func(args)


def monosub_wrapper(args):
  out = citools.monosub(code=args.input.read(),cipher=args.cipher)
  output(args.out,"\n".join(out))
  return

def caesar_wrapper(args):
  if args.shift in ["a","all"]:
    out = citools.all_caesar(code=args.input.read())
    output(args.out,out)
    return
  args.shift = int(args.shift)
  out = citools.caesar(code=args.input.read(), shift=args.shift)
  output(args.out,out)
  return

def freqanalyse_wrapper(args):
  outdict = citools.freqanalyse(code=args.input.read(), letters=args.letters)
  out = str(outdict)
  output(args.out,out)
  return

def keyword_gencipher_wrapper(args):
  out = citools.generate_keyword_cipher(keyword=args.keyword, alphabet=args.alphabet)
  if out == None:
    raise Exception("Keyword needs to have at most one of each letter.")
  output(args.out,out)
  return

def keyword_substitute_wrapper(args):
  out = citools.keyword_substitute(code=args.input.read(),keyword=args.keyword)
  output(args.out,out)
  return

def vigenere_substitute_wrapper(args):
  out = citools.vigenere(code=args.input.read(),keyword=args.keyword,reverse=args.reverse)
  if out == None:
    raise Exception("Keyword needs to have at most one of each letter.")
  output(args.out,out)
  return

def vigenere_key_elimination_wrapper(args):
  out = citools.vigenere_key_elimination(code=args.input.read(), knowntext=args.knowntext, keywordlen=args.keywordlen)
  output(args.out,str(out))
  return


def output(out, text):
  if out == None:
    print(text)
  else:
    out.write(text)
  return

if __name__ == "__main__":
  main()
